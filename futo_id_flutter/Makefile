.PHONY: run clean build-apk test lint format icons proto splash screenshots

run:
	flutter run

clean:
	flutter clean

build-apk:
	flutter build apk --debug

build-release:
	flutter build apk --release

test:
	flutter test

lint:
	flutter analyze \
		lib/main.dart \
		lib/ranges.dart \
		lib/queries.dart \
		lib/synchronizer.dart \
		lib/pages/* \
		lib/api_methods.dart \
		lib/shared_ui.dart \
		lib/handle_validation.dart \
		lib/models.dart \
		lib/models/* \
		test/*

format:
	dart format --set-exit-if-changed \
		lib/main.dart \
		lib/queries.dart \
		lib/ranges.dart \
		lib/synchronizer.dart \
		lib/pages/* \
		lib/api_methods.dart \
		lib/shared_ui.dart \
		lib/handle_validation.dart \
		lib/models.dart \
		lib/models/* \
		test/* \
		test_driver/*

icons:
	dart run flutter_launcher_icons

splash:
	dart run flutter_native_splash:create

proto:
	protoc \
		--proto_path=../../polycentric/proto \
		--dart_out=./lib \
		--experimental_allow_proto3_optional \
		../../polycentric/proto/protocol.proto

screenshots:
	flutter drive \
		--driver=test_driver/integration_test.dart \
		--target=test_driver/main_test.dart

