import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';

import 'package:futo_id_flutter/models.dart' as models;
import 'package:futo_id_flutter/pages/profile.dart' as profile;
import 'package:futo_id_flutter/main.dart' as app;

void main() {
  final IntegrationTestWidgetsFlutterBinding binding =
      IntegrationTestWidgetsFlutterBinding();

  group('end-to-end test', () {
    testWidgets('render home and navigate', (tester) async {
      WidgetsApp.debugAllowBannerOverride = false;
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);

      final db = await app.setupDB();
      final processSecret = await app.createNewIdentity(db);

      await db.transaction((final transaction) async {
        await app.setUsername(transaction, processSecret, "FUTO");

        await app.setDescription(
            transaction,
            processSecret,
            "Computers should belong to you, the people. "
            "We develop and fund technology to give them back.");

        await app.makePlatformClaim(
          transaction,
          processSecret,
          models.ClaimType.claimTypeYouTube,
          "@FUTOTECH",
        );

        await app.makePlatformClaim(
          transaction,
          processSecret,
          models.ClaimType.claimTypeTwitter,
          "@FUTO_TECH",
        );
      });

      final profileImageBytes =
          await rootBundle.load('assets/futo_profile.jpeg');

      await profile.saveAvatar(
        db,
        processSecret,
        profileImageBytes.buffer.asUint8List(),
      );

      final appWidget = await app.createApp();
      await tester.pumpWidget(appWidget);
      await binding.convertFlutterSurfaceToImage();

      await tester.pumpAndSettle();
      await binding.takeScreenshot('profilelist');

      await tester.tap(find.text("FUTO"));
      await tester.pumpAndSettle();
      await binding.takeScreenshot('profilepage');

      await tester.tap(find.text("Make a claim"));
      await tester.pumpAndSettle();
      await binding.takeScreenshot('makeclaim');

      await tester.pageBack();
      await tester.pumpAndSettle();
      await tester.tap(find.text("Twitter"));
      await tester.pumpAndSettle();
      await tester.tap(find.text("Manual"));
      await tester.pumpAndSettle();
      await binding.takeScreenshot('verifyclaim');
    });
  });
}
