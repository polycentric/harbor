import 'package:fixnum/fixnum.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'dart:math';
import 'dart:async';
import 'dart:collection';
import 'dart:typed_data' as dart_typed_data;
import 'package:fixnum/fixnum.dart' as fixnum;
import 'package:path/path.dart' as path;

import 'api_methods.dart' as api_methods;
import 'pages/new_or_import_profile.dart';
import 'models.dart' as models;
import 'protocol.pb.dart' as protocol;
import 'synchronizer.dart' as synchronizer;
import 'queries.dart' as queries;
import 'shared_ui.dart' as shared_ui;
import 'ranges.dart' as ranges;
import 'logger.dart';

import 'package:futo_id_flutter/models/signed_event.dart';
import 'package:futo_id_flutter/models/key.dart';
import 'package:futo_id_flutter/models/process.dart';
import 'package:futo_id_flutter/models/pointer.dart';

dart_typed_data.Uint8List listToConstBuffer(List<int> list) {
  return dart_typed_data.Uint8List.fromList(list).asUnmodifiableView();
}

final UnmodifiableSetView<fixnum.Int64> oAuthClaimTypes = UnmodifiableSetView({
  models.ClaimType.claimTypeDiscord,
  models.ClaimType.claimTypeInstagram,
  models.ClaimType.claimTypeTwitter,
});

class ProcessSecret {
  KeyPair system;
  Process process;

  ProcessSecret(this.system, this.process);
}

bool isOAuthClaim(final fixnum.Int64 claimType) {
  return oAuthClaimTypes.contains(claimType);
}

Future<ProcessSecret> createNewIdentity(final sqflite.Database db) async {
  final keyPair = await KeyPairEd25519.generate();

  return await db.transaction((final transaction) async {
    final processInfo = await importIdentity(transaction, keyPair);

    await setServer(
      transaction,
      processInfo,
      protocol.LWWElementSet_Operation.ADD,
      'https://srv1-prod.polycentric.io',
    );

    await setAuthority(
      transaction,
      processInfo,
      protocol.LWWElementSet_Operation.ADD,
      'https://verifiers.polycentric.io',
    );

    return processInfo;
  });
}

Future<ProcessSecret> importIdentity(
  final sqflite.Transaction transaction,
  final KeyPair keyPair,
) async {
  final process = Process.generate();

  final processSecret = ProcessSecret(keyPair, process);

  await queries.insertProcessSecret(
    transaction,
    processSecret,
  );

  final publicProto = keyPair.publicKey.proto;

  logger.i("imported: ${base64Url.encode(publicProto.writeToBuffer())}");

  return processSecret;
}

Future<String> makeSystemLink(
  final sqflite.Database db,
  final PublicKey system,
) async {
  final servers = await db.transaction((final transaction) async {
    return await loadServerList(transaction, system);
  });

  final systemLink = protocol.URLInfoSystemLink()
    ..system = system.proto
    ..servers.addAll(servers);

  final urlInfo = protocol.URLInfo()
    ..urlType = models.URLInfoType.urlInfoTypeSystemLink
    ..body = systemLink.writeToBuffer();

  return models.urlInfoToLinkSuffix(urlInfo);
}

Future<String> makeEventLink(
  final sqflite.Database db,
  final PublicKey system,
  final Process process,
  final fixnum.Int64 logicalClock,
) async {
  final servers = await db.transaction((final transaction) async {
    return await loadServerList(transaction, system);
  });

  final eventLink = protocol.URLInfoEventLink()
    ..system = system.proto
    ..process = process.proto
    ..logicalClock = logicalClock
    ..servers.addAll(servers);

  final urlInfo = protocol.URLInfo()
    ..urlType = models.URLInfoType.urlInfoTypeEventLink
    ..body = eventLink.writeToBuffer();

  return models.urlInfoToLink(urlInfo);
}

Future<String> makeExportBundle(
  final sqflite.Database db,
  final ProcessSecret processSecret,
) async {
  final events = protocol.Events();

  await db.transaction((final transaction) async {
    final usernameSignedEvent = await queries.loadLatestCRDTByContentType(
      transaction,
      processSecret.system.publicKey,
      models.ContentType.contentTypeUsername,
    );

    if (usernameSignedEvent != null) {
      events.events.add(usernameSignedEvent.proto);
    }

    final serverSignedEvents =
        await queries.loadLatestCRDTSetItemsByContentType(
      transaction,
      processSecret.system.publicKey,
      models.ContentType.contentTypeServer,
    );

    events.events.addAll(serverSignedEvents.map((event) => event.proto));
  });

  final exportBundle = protocol.ExportBundle()
    ..keyPair = processSecret.system.proto
    ..events = events;

  final urlInfo = protocol.URLInfo()
    ..urlType = models.URLInfoType.urlInfoTypeExportBundle
    ..body = exportBundle.writeToBuffer();

  return models.urlInfoToLink(urlInfo);
}

// returns false if identity already exists
// returns true on success
// throws on other error
Future<bool> importExportBundle(
  final sqflite.Database db,
  final protocol.ExportBundle exportBundle,
) async {
  final keyPair = KeyPair.fromProto(exportBundle.keyPair);

  return await db.transaction((final transaction) async {
    final exists = await queries.doesProcessSecretExistForSystem(
      transaction,
      keyPair,
    );

    if (exists == true) {
      return false;
    }

    await importIdentity(transaction, keyPair);

    for (final event in exportBundle.events.events) {
      await ingest(transaction, await SignedEvent.fromProto(event));
    }

    return true;
  });
}

class _GetVouchersComputeArgs {
  final List<String> servers;
  final Pointer claimPointer;

  _GetVouchersComputeArgs(this.servers, this.claimPointer);
}

Future<List<PublicKey>> _getVouchersCompute(
    final _GetVouchersComputeArgs args) async {
  final List<String> servers = args.servers;
  final Pointer claimPointer = args.claimPointer;

  final reference = protocol.Reference()
    ..reference = claimPointer.proto.writeToBuffer()
    ..referenceType = Int64(2);

  final queryReferencesRequestEvents = protocol.QueryReferencesRequestEvents()
    ..fromType = models.ContentType.contentTypeVouch;

  final futures = <Future<protocol.QueryReferencesResponse>>[];
  for (final server in servers) {
    futures.add(api_methods.getQueryReferences(
        server, reference, null, queryReferencesRequestEvents, null, null));
  }

  final List<SignedEvent> vouchEvents = List.empty(growable: true);
  final responses = await Future.wait(futures);
  for (final response in responses) {
    for (final item in response.items) {
      vouchEvents.add(await SignedEvent.fromProto(item.event));
    }
    //TODO: Can we deduplicate the list early?
    //TODO: Handle more than X vouchers by using cursor to get the next page
  }

  final vouchers = <PublicKey>[];
  for (final signedEvent in vouchEvents) {
    final event = protocol.Event.fromBuffer(signedEvent.event);

    final referenceMatches = event.references.any((final r) {
      final referencePointer = Pointer.fromProto(
        protocol.Pointer.fromBuffer(r.reference),
      );
      return claimPointer == referencePointer;
    });

    if (referenceMatches) {
      final system = PublicKey.fromProto(event.system);

      if (!vouchers.contains(system)) {
        vouchers.add(system);
      }
    }
  }

  return vouchers;
}

Future<List<PublicKey>> getVouchersAsync(
    final List<String> servers, final Pointer claimPointer) async {
  final _GetVouchersComputeArgs args =
      _GetVouchersComputeArgs(servers, claimPointer);
  return compute(_getVouchersCompute, args);
}

class _GetProfileComputeArgs {
  final List<String> servers;
  final PublicKey system;

  _GetProfileComputeArgs(this.servers, this.system);
}

Future<models.SystemState> _getProfileCompute(
    final _GetProfileComputeArgs args) async {
  final futures = <Future<List<SignedEvent>>>[];
  for (final server in args.servers) {
    futures.add(api_methods.getQueryLatest(
        server,
        args.system,
        UnmodifiableListView([
          models.ContentType.contentTypeUsername,
          models.ContentType.contentTypeAvatar
        ])));
  }

  final storageTypeSystemState = models.StorageTypeSystemState();
  final responses = await Future.wait(futures);
  for (final response in responses) {
    for (final signedEvent in response) {
      final event = protocol.Event.fromBuffer(signedEvent.event);
      storageTypeSystemState.update(event);
    }
  }

  return models.SystemState.fromStorageTypeSystemState(storageTypeSystemState);
}

Future<models.SystemState> getProfileAsync(
    final List<String> servers, final PublicKey system) async {
  final _GetProfileComputeArgs args = _GetProfileComputeArgs(servers, system);
  return compute(_getProfileCompute, args);
}

Future<void> ingest(
  final sqflite.Transaction transaction,
  final SignedEvent signedEvent,
) async {
  final event = protocol.Event.fromBuffer(signedEvent.event);

  if (await queries.doesEventExist(transaction, event)) {
    logger.d("event already persisted");
    return;
  }

  if (await queries.isEventDeleted(transaction, event)) {
    logger.d("event already deleted");
    return;
  }

  final eventId = await queries.insertEvent(transaction, signedEvent, event);

  if (event.contentType == models.ContentType.contentTypeDelete) {
    final protocol.Delete deleteBody =
        protocol.Delete.fromBuffer(event.content);

    await queries.deleteEventDB(
        transaction, eventId, PublicKey.fromProto(event.system), deleteBody);
  }

  if (event.hasLwwElement()) {
    await queries.insertLWWElement(transaction, eventId, event.lwwElement);
  }

  if (event.hasLwwElementSet()) {
    await queries.insertCRDTSetItem(transaction, eventId, event.lwwElementSet);
  }
}

Future<List<ClaimInfo>> loadClaims(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvents = await queries.loadEventsForSystemByContentType(
    transaction,
    system,
    models.ContentType.contentTypeClaim,
  );

  final List<ClaimInfo> result = [];

  for (final signedEvent in signedEvents) {
    final event = protocol.Event.fromBuffer(signedEvent.event);

    final pointer = await signedEventToPointer(signedEvent);

    result.add(ClaimInfo(pointer, event));
  }

  return result;
}

Future<List<String>> loadCRDTSetItemsAsListOfString(
  final sqflite.Transaction transaction,
  final PublicKey system,
  final fixnum.Int64 contentType,
) async {
  final signedEvents = await queries.loadLatestCRDTSetItemsByContentType(
    transaction,
    system,
    contentType,
  );

  final List<String> result = [];

  for (final signedEvent in signedEvents) {
    final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

    if (!event.hasLwwElementSet()) {
      logger.d("expected hasLWWElementSet");

      continue;
    }

    if (event.contentType != contentType) {
      logger.d("expected ${contentType.toString()} event "
          "but got: ${event.contentType.toString()}");

      continue;
    }

    if (event.lwwElementSet.operation != protocol.LWWElementSet_Operation.ADD) {
      logger.d("expected ADD operation");

      continue;
    }

    result.add(utf8.decode(event.lwwElementSet.value));
  }

  return result;
}

Future<List<String>> loadServerList(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  return await loadCRDTSetItemsAsListOfString(
    transaction,
    system,
    models.ContentType.contentTypeServer,
  );
}

Future<String> loadLatestUsername(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvent = await queries.loadLatestCRDTByContentType(
    transaction,
    system,
    models.ContentType.contentTypeUsername,
  );

  if (signedEvent == null) {
    return 'unknown';
  } else {
    final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

    return utf8.decode(event.lwwElement.value);
  }
}

Future<String> loadLatestDescription(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvent = await queries.loadLatestCRDTByContentType(
    transaction,
    system,
    models.ContentType.contentTypeDescription,
  );

  if (signedEvent == null) {
    return '';
  } else {
    final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

    return utf8.decode(event.lwwElement.value);
  }
}

Future<String> loadLatestStore(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvent = await queries.loadLatestCRDTByContentType(
    transaction,
    system,
    models.ContentType.contentTypeStore,
  );

  if (signedEvent == null) {
    return '';
  } else {
    final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

    return utf8.decode(event.lwwElement.value);
  }
}

Future<String> loadLatestStoreData(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvent = await queries.loadLatestCRDTByContentType(
    transaction,
    system,
    models.ContentType.contentTypeStoreData,
  );

  if (signedEvent == null) {
    return '';
  } else {
    final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

    return utf8.decode(event.lwwElement.value);
  }
}

Future<String> loadLatestPromotion(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvent = await queries.loadLatestCRDTByContentType(
    transaction,
    system,
    models.ContentType.contentTypePromotion,
  );

  if (signedEvent == null) {
    return '';
  } else {
    final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

    return utf8.decode(event.lwwElement.value);
  }
}

Future<List<String>> loadLatestMembershipUrls(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvent = await queries.loadLatestCRDTByContentType(
    transaction,
    system,
    models.ContentType.contentTypeMembershipUrls,
  );

  if (signedEvent == null) {
    return List.empty();
  }

  final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);
  final decodedList =
      jsonDecode(utf8.decode(event.lwwElement.value)) as List<dynamic>;
  return decodedList.map((item) => item as String).toList();
}

Future<List<String>> loadLatestDonationDestinations(
  final sqflite.Transaction transaction,
  final PublicKey system,
) async {
  final signedEvent = await queries.loadLatestCRDTByContentType(
    transaction,
    system,
    models.ContentType.contentTypeDonationDestinations,
  );

  if (signedEvent == null) {
    return List.empty();
  }

  final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);
  final decodedList =
      jsonDecode(utf8.decode(event.lwwElement.value)) as List<dynamic>;
  return decodedList.map((item) => item as String).toList();
}

Future<Image?> loadImage(
  final sqflite.Transaction transaction,
  final String mime,
  final PublicKey system,
  final Process process,
  final List<protocol.Range> sections,
) async {
  final List<int> buffer = [];

  for (final section in sections) {
    for (var i = section.low; i <= section.high; i++) {
      final signedEvent = await queries.loadEvent(
        transaction,
        system,
        process,
        i,
      );

      if (signedEvent == null) {
        return null;
      }

      final event = protocol.Event.fromBuffer(signedEvent.event);

      if (event.contentType != models.ContentType.contentTypeBlobSection) {
        logger.d("expected blob section event but got: "
            "${event.contentType.toString()}");

        return null;
      }

      buffer.addAll(event.content);
    }
  }

  return Image.memory(Uint8List.fromList(buffer));
}

Future<(List<int>, Image?)?> loadLatestAvatar(
    final sqflite.Transaction transaction,
    final PublicKey system,
    final List<int>? lastData,
    final Image? lastImage) async {
  try {
    final signedEvent = await queries.loadLatestCRDTByContentType(
      transaction,
      system,
      models.ContentType.contentTypeAvatar,
    );

    if (signedEvent == null) {
      return null;
    } else {
      final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

      if (event.contentType != models.ContentType.contentTypeAvatar) {
        logger.d("expected blob section event but got: "
            "${event.contentType.toString()}");

        return null;
      }

      if (lastData != null && listEquals(event.lwwElement.value, lastData)) {
        return (lastData, lastImage);
      }

      final protocol.ImageBundle bundle = protocol.ImageBundle.fromBuffer(
        event.lwwElement.value,
      );

      final protocol.ImageManifest? manifest = bundle.imageManifests
          .cast<protocol.ImageManifest?>()
          .firstWhere(
              (final manifest) =>
                  manifest!.width == fixnum.Int64(256) &&
                  manifest.height == fixnum.Int64(256),
              orElse: () => null);

      return (
        event.lwwElement.value,
        manifest != null
            ? await loadImage(transaction, manifest.mime, system,
                Process.fromProto(manifest.process), manifest.sections)
            : null
      );
    }
  } catch (err) {
    return null;
  }
}

Future<(List<int>, Image?)?> loadLatestBanner(
    final sqflite.Transaction transaction,
    final PublicKey system,
    final List<int>? lastData,
    final Image? lastImage) async {
  try {
    final signedEvent = await queries.loadLatestCRDTByContentType(
      transaction,
      system,
      models.ContentType.contentTypeBanner,
    );

    if (signedEvent == null) {
      return null;
    } else {
      final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

      if (event.contentType != models.ContentType.contentTypeBanner) {
        logger.d("expected blob section event but got: "
            "${event.contentType.toString()}");

        return null;
      }

      if (lastData != null && listEquals(event.lwwElement.value, lastData)) {
        return (lastData, lastImage);
      }

      final protocol.ImageBundle bundle = protocol.ImageBundle.fromBuffer(
        event.lwwElement.value,
      );

      final protocol.ImageManifest? manifest = bundle.imageManifests
          .cast<protocol.ImageManifest?>()
          .firstWhere(
              (final manifest) =>
                  manifest!.width == fixnum.Int64(1800) &&
                  manifest.height == fixnum.Int64(600),
              orElse: () => null);

      return (
        event.lwwElement.value,
        manifest != null
            ? await loadImage(transaction, manifest.mime, system,
                Process.fromProto(manifest.process), manifest.sections)
            : null
      );
    }
  } catch (err) {
    return null;
  }
}

Future<(List<int>, Image?)?> loadLatestPromotionBanner(
    final sqflite.Transaction transaction,
    final PublicKey system,
    final List<int>? lastData,
    final Image? lastImage) async {
  try {
    final signedEvent = await queries.loadLatestCRDTByContentType(
      transaction,
      system,
      models.ContentType.contentTypePromotionBanner,
    );

    if (signedEvent == null) {
      return null;
    } else {
      final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

      if (event.contentType != models.ContentType.contentTypePromotionBanner) {
        logger.d("expected blob section event but got: "
            "${event.contentType.toString()}");

        return null;
      }

      if (lastData != null && listEquals(event.lwwElement.value, lastData)) {
        return (lastData, lastImage);
      }

      final protocol.ImageBundle bundle = protocol.ImageBundle.fromBuffer(
        event.lwwElement.value,
      );

      final protocol.ImageManifest? manifest = bundle.imageManifests
          .cast<protocol.ImageManifest?>()
          .firstWhere(
              (final manifest) =>
                  manifest!.width == fixnum.Int64(600) &&
                  manifest.height == fixnum.Int64(200),
              orElse: () => null);

      return (
        event.lwwElement.value,
        manifest != null
            ? await loadImage(transaction, manifest.mime, system,
                Process.fromProto(manifest.process), manifest.sections)
            : null
      );
    }
  } catch (err) {
    return null;
  }
}

Future<Pointer> saveEvent(final sqflite.Transaction transaction,
    final ProcessSecret processInfo, final protocol.Event event) async {
  final clock = await queries.loadLatestClock(
    transaction,
    processInfo.system.publicKey,
    processInfo.process,
  );

  event.system = processInfo.system.publicKey.proto;
  event.process = processInfo.process.proto;
  event.logicalClock = fixnum.Int64(clock);
  event.vectorClock = protocol.VectorClock();
  event.indices = protocol.Indices();
  event.unixMilliseconds = fixnum.Int64(DateTime.now().millisecondsSinceEpoch);

  final signedEvent = await SignedEvent.fromEvent(event, processInfo.system);

  await ingest(transaction, signedEvent);

  return await signedEventToPointer(signedEvent);
}

Future<void> deleteEvent(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final Pointer pointer,
) async {
  final signedEvent = await queries.loadEvent(
    transaction,
    pointer.system,
    pointer.process,
    pointer.logicalClock,
  );

  if (signedEvent == null) {
    logger.d("cannot delete event that does not exist");
    return;
  } else {
    final protocol.Event event = protocol.Event.fromBuffer(signedEvent.event);

    final delete = protocol.Delete()
      ..process = pointer.process.proto
      ..logicalClock = pointer.logicalClock
      ..indices = event.indices
      ..unixMilliseconds = event.unixMilliseconds
      ..contentType = event.contentType;

    final protocol.Event deleteEvent = protocol.Event()
      ..contentType = models.ContentType.contentTypeDelete
      ..content = delete.writeToBuffer();

    await saveEvent(transaction, processInfo, deleteEvent);
  }
}

Future<List<ranges.Range>> publishBlob(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String mime,
  final List<int> bytes,
) async {
  final List<ranges.Range> result = [];

  const maxBytes = 1024 * 512;

  for (var i = 0; i < bytes.length; i += maxBytes) {
    final sectionEvent = protocol.Event()
      ..contentType = models.ContentType.contentTypeBlobSection
      ..content = bytes.sublist(i, i + min(maxBytes, bytes.length - i));

    final pointer = await saveEvent(transaction, processInfo, sectionEvent);

    ranges.insert(result, pointer.logicalClock);
  }

  return result;
}

Future<void> setCRDT(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final fixnum.Int64 contentType,
  final Uint8List bytes,
) async {
  final lwwElement = protocol.LWWElement()
    ..unixMilliseconds = fixnum.Int64(DateTime.now().millisecondsSinceEpoch)
    ..value = bytes;

  final protocol.Event event = protocol.Event()
    ..contentType = contentType
    ..lwwElement = lwwElement;

  await saveEvent(transaction, processInfo, event);
}

Future<void> setAvatar(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final protocol.ImageBundle imageBundle,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeAvatar,
    imageBundle.writeToBuffer(),
  );
}

Future<void> setUsername(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String username,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeUsername,
    Uint8List.fromList(utf8.encode(username)),
  );
}

Future<void> setDescription(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String description,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeDescription,
    Uint8List.fromList(utf8.encode(description)),
  );
}

Future<void> setStore(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String storeLink,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeStore,
    Uint8List.fromList(utf8.encode(storeLink)),
  );
}

Future<void> setStoreData(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String storeLink,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeStoreData,
    Uint8List.fromList(utf8.encode(storeLink)),
  );
}

Future<void> setBanner(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final protocol.ImageBundle imageBundle,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeBanner,
    imageBundle.writeToBuffer(),
  );
}

Future<void> setPromotionBanner(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final protocol.ImageBundle imageBundle,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypePromotionBanner,
    imageBundle.writeToBuffer(),
  );
}

Future<void> setPromotion(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String storeLink,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypePromotion,
    Uint8List.fromList(utf8.encode(storeLink)),
  );
}

Future<void> setMembershipUrls(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final List<String> membershipUrls,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeMembershipUrls,
    Uint8List.fromList(utf8.encode(jsonEncode(membershipUrls))),
  );
}

Future<void> setDonationDestinations(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final List<String> donationDestinations,
) async {
  await setCRDT(
    transaction,
    processInfo,
    models.ContentType.contentTypeDonationDestinations,
    Uint8List.fromList(utf8.encode(jsonEncode(donationDestinations))),
  );
}

Future<void> makeClaim(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String claimText,
) async {
  final claim = models.claimIdentifier(
    models.ClaimType.claimTypeGeneric,
    claimText,
  );

  final protocol.Event event = protocol.Event()
    ..contentType = models.ContentType.contentTypeClaim
    ..content = claim.writeToBuffer();

  await saveEvent(transaction, processInfo, event);
}

Future<void> setCRDTSetItem(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final fixnum.Int64 contentType,
  final protocol.LWWElementSet_Operation operation,
  final Uint8List value,
) async {
  final lwwElementSet = protocol.LWWElementSet()
    ..unixMilliseconds = fixnum.Int64(DateTime.now().millisecondsSinceEpoch)
    ..value = value
    ..operation = operation;

  final protocol.Event event = protocol.Event()
    ..contentType = contentType
    ..lwwElementSet = lwwElementSet;

  await saveEvent(transaction, processInfo, event);
}

Future<void> setServer(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final protocol.LWWElementSet_Operation operation,
  final String server,
) async {
  await setCRDTSetItem(
    transaction,
    processInfo,
    models.ContentType.contentTypeServer,
    operation,
    Uint8List.fromList(utf8.encode(server)),
  );
}

Future<void> setAuthority(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final protocol.LWWElementSet_Operation operation,
  final String authority,
) async {
  await setCRDTSetItem(
    transaction,
    processInfo,
    models.ContentType.contentTypeAuthority,
    operation,
    Uint8List.fromList(utf8.encode(authority)),
  );
}

Future<ClaimInfo> persistClaim(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final protocol.Claim claim,
) async {
  final protocol.Event event = protocol.Event()
    ..contentType = models.ContentType.contentTypeClaim
    ..content = claim.writeToBuffer();

  final pointer = await saveEvent(transaction, processInfo, event);

  return ClaimInfo(pointer, event);
}

Future<ClaimInfo> makePlatformClaim(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final fixnum.Int64 claimType,
  final String account,
) async {
  return await persistClaim(
    transaction,
    processInfo,
    models.claimIdentifier(claimType, account),
  );
}

Future<ClaimInfo> makeOccupationClaim(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final String? organization,
  final String? role,
  final String? location,
) async {
  return await persistClaim(
    transaction,
    processInfo,
    models.claimOccupation(organization, role, location),
  );
}

Future<void> makeVouch(
  final sqflite.Transaction transaction,
  final ProcessSecret processInfo,
  final Pointer pointer,
) async {
  final reference = protocol.Reference()
    ..referenceType = fixnum.Int64(2)
    ..reference = pointer.proto.writeToBuffer();

  final protocol.Event event = protocol.Event()
    ..contentType = models.ContentType.contentTypeVouch
    ..references.add(reference);

  await saveEvent(transaction, processInfo, event);
}

Future<void> main() async {
  runApp(await createApp());
}

Future<Widget> createApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light); // light color time text

  final provider = await setupModel();
  await provider.mLoadIdentities();

  return NeopassApp(polycentricModel: provider);
}

class ClaimInfo {
  final Pointer pointer;
  final protocol.Event event;
  protocol.Claim claim = protocol.Claim();

  ClaimInfo(this.pointer, this.event) {
    claim = protocol.Claim.fromBuffer(event.content);
  }

  String? getField(final fixnum.Int64 key) {
    for (final field in claim.claimFields) {
      if (field.key == key) {
        return field.value;
      }
    }

    return null;
  }
}

class ProcessInfo {
  final ProcessSecret processSecret;
  final String username;
  final List<ClaimInfo> claims;
  final List<int>? avatarData;
  final Image? avatar;
  final List<int>? bannerData;
  final Image? banner;
  final String promotion;
  final List<int>? promotionBannerData;
  final Image? promotionBanner;
  final String description;
  final String store;
  final String storeData;
  final List<String> servers;
  final List<String> membershipUrls;
  final List<String> donationDestinations;

  ProcessInfo(
      this.processSecret,
      this.username,
      this.claims,
      this.avatarData,
      this.avatar,
      this.bannerData,
      this.banner,
      this.promotion,
      this.promotionBannerData,
      this.promotionBanner,
      this.description,
      this.store,
      this.storeData,
      this.servers,
      this.membershipUrls,
      this.donationDestinations);
}

class PolycentricModel extends ChangeNotifier {
  final sqflite.Database db;
  List<ProcessInfo> identities = [];
  int counter = 0;

  PolycentricModel(this.db);

  Future<ProcessInfo> _loadIdentity(ProcessInfo? lastProcessInfo,
      ProcessSecret identity, PublicKey system) async {
    logger.i("reloading identity ${system.toString()}");

    ProcessInfo? processInfo;
    await db.transaction((final transaction) async {
      final username = await loadLatestUsername(
        transaction,
        system,
      );

      final description = await loadLatestDescription(
        transaction,
        system,
      );

      final store = await loadLatestStore(
        transaction,
        system,
      );

      final storeData = await loadLatestStoreData(
        transaction,
        system,
      );

      final avatarPair = await loadLatestAvatar(transaction, system,
          lastProcessInfo?.avatarData, lastProcessInfo?.avatar);

      final bannerPair = await loadLatestBanner(transaction, system,
          lastProcessInfo?.bannerData, lastProcessInfo?.banner);

      final promotion = await loadLatestPromotion(
        transaction,
        system,
      );

      final promotionBannerPair = await loadLatestPromotionBanner(
          transaction,
          system,
          lastProcessInfo?.promotionBannerData,
          lastProcessInfo?.promotionBanner);

      final membershipUrls = await loadLatestMembershipUrls(
        transaction,
        system,
      );

      final donationDestinations = await loadLatestDonationDestinations(
        transaction,
        system,
      );

      final servers = await loadServerList(transaction, system);
      final claims = await loadClaims(transaction, system);

      processInfo = ProcessInfo(
          identity,
          username,
          claims,
          avatarPair?.$1,
          avatarPair?.$2,
          bannerPair?.$1,
          bannerPair?.$2,
          promotion,
          promotionBannerPair?.$1,
          promotionBannerPair?.$2,
          description,
          store,
          storeData,
          servers,
          membershipUrls,
          donationDestinations);
    });

    return processInfo!;
  }

  Future<void> mLoadIdentities() async {
    final count = ++counter;
    final identities = await queries.loadProcessSecrets(db);
    if (count != counter) {
      //return if cancelled
      return;
    }

    final List<ProcessInfo> loadedIdentities = [];
    for (final i in identities) {
      final identity = i;

      if (count != counter) {
        //return if cancelled
        return;
      }

      final existingProcessInfo =
          this.identities.cast<ProcessInfo?>().firstWhere((v) {
        if (identity.system.publicKey.keyType !=
            v!.processSecret.system.publicKey.keyType) {
          return false;
        }
        if (!listEquals(identity.system.publicKey.bytes,
            v.processSecret.system.publicKey.bytes)) {
          return false;
        }

        return true;
      }, orElse: () {
        return null;
      });

      final processInfo = await _loadIdentity(
          existingProcessInfo, identity, identity.system.publicKey);
      if (count != counter) {
        //return if cancelled
        return;
      }

      loadedIdentities.add(processInfo);

      synchronizer
          .fullyBackfillClient(db, identity.system.publicKey)
          .then((progress) async {
        logger.i("backfill client completed (progress: $progress)");

        if (count != counter || !progress) {
          //return if cancelled
          return;
        }

        final identityIndex = this.identities.indexOf(processInfo);
        final updatedIdentity = await _loadIdentity(existingProcessInfo,
            identity, processInfo.processSecret.system.publicKey);
        if (count != counter) {
          //return if cancelled
          return;
        }

        this.identities[identityIndex] = updatedIdentity;
        notifyListeners();
      }).catchError((Object err) {
        logger.e("backfill client failed", error: err);
      });

      synchronizer
          .fullyBackfillServers(db, processInfo.processSecret.system.publicKey)
          .then((progress) {
        logger.i("backfill server completed (progress: $progress)");
      }).catchError((Object err) {
        logger.e("backfill server failed", error: err);
      });
    }

    if (count != counter) {
      //return if cancelled
      return;
    }

    this.identities = loadedIdentities;
    notifyListeners();
  }
}

Future<sqflite.Database> setupDB() async {
  final name = path.join(await sqflite.getDatabasesPath(), 'futo_id1.db');
  return await queries.createDB(name);
}

Future<PolycentricModel> setupModel() async {
  final name = path.join(await sqflite.getDatabasesPath(), 'futo_id1.db');
  return PolycentricModel(await queries.createDB(name));
}

class NeopassApp extends StatelessWidget {
  final PolycentricModel polycentricModel;
  const NeopassApp({super.key, required this.polycentricModel});

  @override
  Widget build(BuildContext context) {
    final initialPage = polycentricModel.identities.isEmpty
        ? const NewOrImportProfilePage()
        : const NewOrImportProfilePage();

    final darkTheme = ThemeData.dark();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<PolycentricModel>(
          create: (context) => polycentricModel,
        ),
      ],
      child: MaterialApp(
        title: 'FUTO ID',
        debugShowCheckedModeBanner: false,
        theme: darkTheme.copyWith(
            brightness: Brightness.dark,
            primaryColor: const Color(0xFF2D63ED),
            colorScheme: const ColorScheme.dark(
              primary: Color(0xFF2D63ED),
              secondary: Colors.white,
            ),
            canvasColor: Colors.black,
            scaffoldBackgroundColor: Colors.black,
            dialogBackgroundColor: shared_ui.buttonColor,
            textTheme: darkTheme.textTheme.apply(fontFamily: 'inter'),
            appBarTheme: const AppBarTheme(
              backgroundColor: Colors.black,
            ),
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all(shared_ui.buttonColor),
                  elevation: WidgetStateProperty.all(0)),
            ),
            outlinedButtonTheme: OutlinedButtonThemeData(
                style: ButtonStyle(
                    side: WidgetStateProperty.all(const BorderSide(
              color: Colors.transparent,
            )))),
            iconButtonTheme: IconButtonThemeData(
                style: ButtonStyle(
                    side: WidgetStateProperty.all(const BorderSide(
              color: Colors.transparent,
            )))),
            textButtonTheme: TextButtonThemeData(
                style: ButtonStyle(
                    foregroundColor: WidgetStateProperty.all(Colors.white))),
            tabBarTheme: const TabBarTheme(indicatorColor: Colors.white)),
        home: SafeArea(child: initialPage),
      ),
    );
  }
}
