import 'package:fixnum/fixnum.dart' as fixnum;

import 'package:futo_id_flutter/protocol.pb.dart' as protocol;
import 'package:futo_id_flutter/models/key.dart';
import 'package:futo_id_flutter/models/process.dart';
import 'package:futo_id_flutter/models/digest.dart';
import 'package:futo_id_flutter/models/signed_event.dart';

class Pointer {
  final PublicKey _system;
  final Process _process;
  final fixnum.Int64 _logicalClock;
  final Digest _digest;

  Pointer(this._system, this._process, this._logicalClock, this._digest);

  static Pointer fromProto(final protocol.Pointer proto) {
    return Pointer(
      PublicKey.fromProto(proto.system),
      Process.fromProto(proto.process),
      proto.logicalClock,
      Digest.fromProto(proto.eventDigest),
    );
  }

  protocol.Pointer get proto {
    return protocol.Pointer()
      ..system = _system.proto
      ..process = _process.proto
      ..logicalClock = _logicalClock
      ..eventDigest = _digest.proto;
  }

  PublicKey get system {
    return _system;
  }

  Process get process {
    return _process;
  }

  fixnum.Int64 get logicalClock {
    return _logicalClock;
  }

  Digest get digest {
    return _digest;
  }

  @override
  bool operator ==(final Object other) {
    if (other is Pointer) {
      return _system == other.system &&
          _process == other.process &&
          _logicalClock == other.logicalClock &&
          _digest == other.digest;
    }
    return false;
  }

  @override
  int get hashCode => Object.hash(_system, _process, _logicalClock, _digest);
}

Future<Pointer> signedEventToPointer(final SignedEvent signedEvent) async {
  final event = protocol.Event.fromBuffer(signedEvent.event);

  return Pointer(
    PublicKey.fromProto(event.system),
    Process.fromProto(event.process),
    event.logicalClock,
    await DigestSHA256.hash(signedEvent.event),
  );
}
