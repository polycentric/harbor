import 'dart:typed_data' as dart_typed_data;

import 'package:flutter/foundation.dart' as flutter_foundation;
import 'package:fixnum/fixnum.dart' as fixnum;
import 'package:cryptography/cryptography.dart' as cryptography;

import 'package:futo_id_flutter/protocol.pb.dart' as protocol;

dart_typed_data.Uint8List _listToConstBuffer(final List<int> list) {
  return dart_typed_data.Uint8List.fromList(list).asUnmodifiableView();
}

abstract class PublicKey {
  final fixnum.Int64 _keyType;
  final dart_typed_data.Uint8List _key;

  PublicKey._(this._keyType, this._key);

  @override
  bool operator ==(final Object other) {
    if (other is PublicKey) {
      return _keyType == other.keyType &&
          flutter_foundation.listEquals(_key, other.bytes);
    }
    return false;
  }

  @override
  int get hashCode => Object.hash(_keyType, _key);

  static PublicKey fromProto(final protocol.PublicKey key) {
    if (key.keyType == fixnum.Int64(1)) {
      return PublicKeyEd25519(
        _listToConstBuffer(key.key),
      );
    } else {
      throw const FormatException();
    }
  }

  fixnum.Int64 get keyType {
    return _keyType;
  }

  dart_typed_data.Uint8List get bytes {
    return _key;
  }

  protocol.PublicKey get proto {
    return protocol.PublicKey()
      ..keyType = _keyType
      ..key = _key;
  }

  Future<bool> validateSignature(
    final dart_typed_data.Uint8List signature,
    final dart_typed_data.Uint8List buffer,
  );
}

class PublicKeyEd25519 extends PublicKey {
  final cryptography.SimplePublicKey _ed25519Key;

  PublicKeyEd25519.fromSimplePublicKey(
    final cryptography.SimplePublicKey key,
  )   : _ed25519Key = key,
        super._(
          fixnum.Int64(1),
          _listToConstBuffer(key.bytes),
        );

  PublicKeyEd25519(
    final dart_typed_data.Uint8List key,
  )   : _ed25519Key = cryptography.SimplePublicKey(
          key,
          type: cryptography.KeyPairType.ed25519,
        ),
        super._(fixnum.Int64(1), key);

  @override
  Future<bool> validateSignature(
    final dart_typed_data.Uint8List signature,
    final dart_typed_data.Uint8List buffer,
  ) async {
    final signature2 = cryptography.Signature(
      signature,
      publicKey: _ed25519Key,
    );

    return await cryptography.Ed25519().verify(
      buffer,
      signature: signature2,
    );
  }
}

abstract class KeyPair {
  final PublicKey _publicKey;
  final dart_typed_data.Uint8List _privateKey;

  KeyPair._(this._publicKey, this._privateKey);

  static KeyPair fromProto(final protocol.KeyPair key) {
    if (key.keyType == fixnum.Int64(1)) {
      return KeyPairEd25519.construct(
        _listToConstBuffer(key.publicKey),
        _listToConstBuffer(key.privateKey),
      );
    } else {
      throw const FormatException();
    }
  }

  PublicKey get publicKey {
    return _publicKey;
  }

  dart_typed_data.Uint8List get privateKey {
    return _privateKey;
  }

  protocol.KeyPair get proto {
    return protocol.KeyPair()
      ..keyType = _publicKey.keyType
      ..publicKey = _publicKey.bytes
      ..privateKey = _privateKey;
  }

  Future<dart_typed_data.Uint8List> sign(
    final dart_typed_data.Uint8List buffer,
  );
}

class KeyPairEd25519 extends KeyPair {
  final cryptography.SimpleKeyPair _keyPair;

  KeyPairEd25519._(
    super.publicKey,
    super.privateKey,
    final cryptography.SimpleKeyPair keyPair,
  )   : _keyPair = keyPair,
        super._();

  static KeyPairEd25519 construct(
    final dart_typed_data.Uint8List publicKey,
    final dart_typed_data.Uint8List privateKey,
  ) {
    final public = cryptography.SimplePublicKey(
      publicKey,
      type: cryptography.KeyPairType.ed25519,
    );

    final keyPair = cryptography.SimpleKeyPairData(
      privateKey,
      publicKey: public,
      type: cryptography.KeyPairType.ed25519,
    );

    return KeyPairEd25519._(
        PublicKeyEd25519.fromSimplePublicKey(public), privateKey, keyPair);
  }

  static Future<KeyPairEd25519> generate() async {
    final algorithm = cryptography.Ed25519();
    final keyPair = await algorithm.newKeyPair();
    final public = await keyPair.extractPublicKey();
    final privateKey = await keyPair.extractPrivateKeyBytes();

    return KeyPairEd25519._(PublicKeyEd25519.fromSimplePublicKey(public),
        _listToConstBuffer(privateKey), keyPair);
  }

  @override
  Future<dart_typed_data.Uint8List> sign(
    final dart_typed_data.Uint8List buffer,
  ) async {
    final signature = (await cryptography.Ed25519().sign(
      buffer,
      keyPair: _keyPair,
    ));

    return _listToConstBuffer(signature.bytes);
  }
}
