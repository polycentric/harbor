import 'dart:typed_data' as dart_typed_data;

import 'package:flutter/foundation.dart' as flutter_foundation;

import 'package:futo_id_flutter/main.dart' as main;
import 'package:futo_id_flutter/models/key.dart';
import 'package:futo_id_flutter/protocol.pb.dart' as protocol;

class SignedEvent {
  final dart_typed_data.Uint8List _signature;
  final dart_typed_data.Uint8List _event;

  SignedEvent._(
    this._signature,
    this._event,
  );

  @override
  bool operator ==(final Object other) {
    if (other is SignedEvent) {
      return flutter_foundation.listEquals(_signature, other.signature) &&
          flutter_foundation.listEquals(_event, other.event);
    }
    return false;
  }

  @override
  int get hashCode => Object.hash(_signature, _event);

  dart_typed_data.Uint8List get event {
    return _event;
  }

  dart_typed_data.Uint8List get signature {
    return _signature;
  }

  protocol.SignedEvent get proto {
    return protocol.SignedEvent()
      ..signature = _signature
      ..event = _event;
  }

  static Future<SignedEvent> fromProto(
    final protocol.SignedEvent proto,
  ) async {
    return await SignedEvent.construct(
      main.listToConstBuffer(proto.signature),
      main.listToConstBuffer(proto.event),
    );
  }

  static Future<SignedEvent> fromEvent(
    final protocol.Event event,
    final KeyPair system,
  ) async {
    final encoded = event.writeToBuffer();

    final signature = await system.sign(
      encoded,
    );

    return SignedEvent._(signature, main.listToConstBuffer(encoded));
  }

  static Future<SignedEvent> construct(
    final dart_typed_data.Uint8List signature,
    final dart_typed_data.Uint8List event,
  ) async {
    final eventProto = protocol.Event.fromBuffer(event);
    final publicKey = PublicKey.fromProto(eventProto.system);

    final validSignature = await publicKey.validateSignature(
      signature,
      event,
    );

    if (!validSignature) {
      throw const FormatException();
    }

    return SignedEvent._(signature, event);
  }
}
