import 'dart:typed_data' as dart_typed_data;
import 'dart:math' as dart_math;
import 'package:flutter/foundation.dart' as flutter_foundation;

import 'package:futo_id_flutter/protocol.pb.dart' as protocol;

List<int> _validate(final List<int> process) {
  if (process.length != 16) {
    throw const FormatException();
  }

  return process;
}

class Process {
  final dart_typed_data.Uint8List _process;

  Process(final List<int> process)
      : _process = dart_typed_data.Uint8List.fromList(_validate(process))
            .asUnmodifiableView();

  @override
  bool operator ==(final Object other) {
    if (other is Process) {
      return flutter_foundation.listEquals(_process, other.bytes);
    }
    return false;
  }

  @override
  int get hashCode => _process.hashCode;

  Process.fromProto(final protocol.Process proto) : this(proto.process);

  dart_typed_data.Uint8List get bytes {
    return _process;
  }

  protocol.Process get proto {
    return protocol.Process()..process = _process;
  }

  static Process generate() {
    return Process(List<int>.generate(
        16, (final i) => dart_math.Random.secure().nextInt(256)));
  }
}
