import 'dart:typed_data' as dart_typed_data;

import 'package:fixnum/fixnum.dart' as fixnum;
import 'package:flutter/foundation.dart' as flutter_foundation;
import 'package:cryptography/cryptography.dart' as cryptography;

import 'package:futo_id_flutter/main.dart' as main;
import 'package:futo_id_flutter/protocol.pb.dart' as protocol;

abstract class Digest {
  final fixnum.Int64 _digestType;
  final dart_typed_data.Uint8List _digest;

  Digest._(this._digestType, this._digest);

  @override
  bool operator ==(final Object other) {
    if (other is Digest) {
      return _digestType == other.digestType &&
          flutter_foundation.listEquals(_digest, other.digest);
    }
    return false;
  }

  @override
  int get hashCode => Object.hash(_digestType, _digest);

  static Digest fromProto(final protocol.Digest proto) {
    if (proto.digestType == fixnum.Int64(1)) {
      return DigestSHA256(
        main.listToConstBuffer(proto.digest),
      );
    } else {
      throw const FormatException();
    }
  }

  static Future<Digest> computeDigest(
    final fixnum.Int64 digestType,
    final dart_typed_data.Uint8List buffer,
  ) async {
    if (digestType == fixnum.Int64(1)) {
      return await DigestSHA256.hash(buffer);
    } else {
      throw const FormatException();
    }
  }

  fixnum.Int64 get digestType {
    return _digestType;
  }

  dart_typed_data.Uint8List get digest {
    return _digest;
  }

  protocol.Digest get proto {
    return protocol.Digest()
      ..digestType = _digestType
      ..digest = _digest;
  }
}

dart_typed_data.Uint8List _validateSHA256(
  final dart_typed_data.Uint8List digest,
) {
  if (digest.length != 32) {
    throw const FormatException();
  }

  return digest;
}

class DigestSHA256 extends Digest {
  DigestSHA256(
    final dart_typed_data.Uint8List digest,
  ) : super._(
          fixnum.Int64(1),
          _validateSHA256(digest),
        );

  static Future<DigestSHA256> hash(
    final dart_typed_data.Uint8List buffer,
  ) async {
    final digest = await cryptography.Sha256().hash(buffer);
    return DigestSHA256(main.listToConstBuffer(digest.bytes));
  }
}
