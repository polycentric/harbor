import 'dart:collection' as dart_collection;

import 'package:flutter/foundation.dart' as foundation;
import 'package:sqflite/sqflite.dart' as sqflite;

import 'main.dart' as main;
import 'protocol.pb.dart' as protocol;
import 'api_methods.dart' as api_methods;
import 'ranges.dart' as ranges;
import 'queries.dart' as queries;
import 'logger.dart';

import 'package:futo_id_flutter/models/signed_event.dart';
import 'package:futo_id_flutter/models/process.dart';
import 'package:futo_id_flutter/models/key.dart';

bool processDeepEqual(final protocol.Process x, final protocol.Process y) {
  return foundation.listEquals(x.process, y.process);
}

List<ranges.Range> protocolRangesToRanges(final List<protocol.Range> x) {
  return x.map((item) => ranges.Range(low: item.low, high: item.high)).toList();
}

List<protocol.Range> rangesToProtocolRanges(final List<ranges.Range> x) {
  return x.map((item) {
    final range = protocol.Range();
    range.low = item.low;
    range.high = item.high;
    return range;
  }).toList();
}

Future<bool> backfillClientSingle(
  final sqflite.Database db,
  final PublicKey system,
  final String server,
) async {
  logger.d('backfillClientSingle $server');

  final serverRangesForSystem = await api_methods.getRanges(server, system);
  final clientRangesForSystem = await db.transaction((transaction) async {
    return await queries.rangesForSystem(transaction, system);
  });

  var progress = false;

  for (final serverRangesForProcess
      in serverRangesForSystem.rangesForProcesses) {
    final serverRanges = protocolRangesToRanges(serverRangesForProcess.ranges);
    List<ranges.Range> clientRanges = [];

    for (final clientRangesForProcess
        in clientRangesForSystem.rangesForProcesses) {
      if (processDeepEqual(
        clientRangesForProcess.process,
        serverRangesForProcess.process,
      )) {
        clientRanges = protocolRangesToRanges(clientRangesForProcess.ranges);
        break;
      }
    }

    final clientNeeds = ranges.subtractRange(serverRanges, clientRanges);

    if (clientNeeds.isEmpty) {
      break;
    }

    final rangesForProcess = protocol.RangesForProcess()
      ..process = serverRangesForProcess.process
      ..ranges.addAll(rangesToProtocolRanges(clientNeeds));

    final request = protocol.RangesForSystem()
      ..rangesForProcesses.add(rangesForProcess);

    final events = await api_methods.getEvents(server, system, request);

    if (events.isEmpty) {
      continue;
    }

    progress = true;

    for (final event in events) {
      await db.transaction((transaction) async {
        await main.ingest(transaction, event);
      });
    }
  }

  return progress;
}

Future<bool> fullyBackfillClient(
  final sqflite.Database db,
  final PublicKey system,
) async {
  bool progress = false;
  while (true) {
    //TODO: backfillClient will keep updating the same servers even if the last call progress was false
    if (await backfillClient(db, system) == false) {
      break;
    }

    progress = true;
  }

  return progress;
}

Future<bool> backfillClient(
  final sqflite.Database db,
  final PublicKey system,
) async {
  final servers = await db.transaction((transaction) async {
    return await main.loadServerList(transaction, system);
  });

  //Update multiple servers in parallel
  final results = await Future.wait(servers.map((server) async {
    try {
      return await backfillClientSingle(db, system, server);
    } catch (e) {
      // If an error occurs, log it and return false for this server.
      logger.e('Error backfilling client $server: $e');
      return false;
    }
  }),
      eagerError:
          false); // Setting eagerError to false to wait for all futures, even if some fail.

  final progress = results.any((e) => e);
  logger.d('backfillclient progress $progress');
  return progress;
}

Future<bool> backfillServerSingle(
  final sqflite.Database db,
  final PublicKey system,
  final String server,
) async {
  logger.d('backfillServerSingle $server');

  final serverRangesForSystem = await api_methods.getRanges(server, system);
  final clientRangesForSystem = await db.transaction((transaction) async {
    return await queries.rangesForSystem(transaction, system);
  });

  var progress = false;

  for (final clientRangesForProcess
      in clientRangesForSystem.rangesForProcesses) {
    final clientRanges = protocolRangesToRanges(clientRangesForProcess.ranges);
    List<ranges.Range> serverRanges = [];

    for (final serverRangesForProcess
        in serverRangesForSystem.rangesForProcesses) {
      if (processDeepEqual(
        clientRangesForProcess.process,
        serverRangesForProcess.process,
      )) {
        serverRanges = protocolRangesToRanges(serverRangesForProcess.ranges);
        break;
      }
    }

    final serverNeeds = ranges.subtractRange(clientRanges, serverRanges);

    if (serverNeeds.isEmpty) {
      continue;
    }

    final List<SignedEvent> payload = [];

    await db.transaction((transaction) async {
      for (final range in serverNeeds) {
        logger.d('loading range $range');
        payload.addAll(
          await queries.loadEventRange(
            transaction,
            system,
            Process.fromProto(clientRangesForProcess.process),
            range,
          ),
        );
      }
    });

    logger.d('sending count ${payload.length}');

    await api_methods.postEvents(
        server, dart_collection.UnmodifiableListView(payload));

    progress = true;
  }

  return progress;
}

Future<bool> fullyBackfillServers(
  final sqflite.Database db,
  final PublicKey system,
) async {
  bool progress = false;
  while (true) {
    //TODO: backfillServers will keep updating the same servers even if the last call progress was false
    if (await backfillServers(db, system) == false) {
      break;
    }

    progress = true;
  }

  return progress;
}

Future<bool> backfillServers(
  final sqflite.Database db,
  final PublicKey system,
) async {
  final servers = await db.transaction((transaction) async {
    return await main.loadServerList(transaction, system);
  });

  //Update multiple servers in parallel
  final results = await Future.wait(servers.map((server) async {
    try {
      return await backfillServerSingle(db, system, server);
    } catch (e) {
      // If an error occurs, log it and return false for this server.
      logger.e('Error backfilling server $server: $e');
      return false;
    }
  }),
      eagerError:
          false); // Setting eagerError to false to wait for all futures, even if some fail.

  final progress = results.any((e) => e);
  logger.d('backfillServers progress $progress');
  return progress;
}
