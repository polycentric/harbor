import 'dart:collection' as dart_collection;

import 'package:test/test.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'package:sqflite_common_ffi/sqflite_ffi.dart' as sqflite_ffi;
import 'package:fixnum/fixnum.dart' as fixnum;

import 'package:futo_id_flutter/queries.dart' as queries;
import 'package:futo_id_flutter/protocol.pb.dart' as protocol;
import 'package:futo_id_flutter/models.dart' as models;
import 'package:futo_id_flutter/models/process.dart';
import 'package:futo_id_flutter/models/key.dart';
import 'package:futo_id_flutter/models/signed_event.dart';

void sqfliteTestInit() {
  sqflite_ffi.sqfliteFfiInit();
  sqflite.databaseFactory = sqflite_ffi.databaseFactoryFfi;
}

class TestProcessInfo {
  KeyPair system;
  Process process;

  TestProcessInfo(this.system, this.process);
}

Future<TestProcessInfo> generateTestProcessInfo() async {
  final system = await KeyPairEd25519.generate();
  final process = Process.generate();

  return TestProcessInfo(system, process);
}

int compareBuffers(dart_collection.UnmodifiableListView<int> a,
    dart_collection.UnmodifiableListView<int> b) {
  for (var i = 0; i < a.length && i < b.length; i += 1) {
    if (a[i] < b[i]) {
      return -1;
    } else if (a[i] > b[i]) {
      return 1;
    }
  }

  if (a.length < b.length) {
    return -1;
  } else if (a.length > b.length) {
    return 1;
  }

  return 0;
}

void main() {
  sqfliteTestInit();

  group('queries', () {
    test('get event that does not exist', () async {
      final testInfo = await generateTestProcessInfo();

      final db = await queries.createDB(sqflite.inMemoryDatabasePath);

      await db.transaction((final transaction) async {
        final loadedEvent = await queries.loadEvent(
          transaction,
          testInfo.system.publicKey,
          testInfo.process,
          fixnum.Int64(5),
        );

        expect(loadedEvent, null);
      });
    });

    test('persist and get event', () async {
      final testInfo = await generateTestProcessInfo();

      final event = protocol.Event()
        ..system = testInfo.system.publicKey.proto
        ..process = testInfo.process.proto
        ..logicalClock = fixnum.Int64(2)
        ..contentType = models.ContentType.contentTypePost;

      final signedEvent = await SignedEvent.fromEvent(event, testInfo.system);

      final db = await queries.createDB(sqflite.inMemoryDatabasePath);

      await db.transaction((final transaction) async {
        await queries.insertEvent(
          transaction,
          signedEvent,
          event,
        );

        final loadedEvent = await queries.loadEvent(
          transaction,
          testInfo.system.publicKey,
          testInfo.process,
          event.logicalClock,
        );

        expect(loadedEvent, signedEvent);
      });
    });

    test('ranges for system', () async {
      final testInfo1 = await generateTestProcessInfo();
      final testInfo2 = await generateTestProcessInfo();
      final testInfo1AltProcess = await generateTestProcessInfo();

      Future<void> insertTestEvent(
        final sqflite.Transaction transaction,
        final TestProcessInfo testInfo,
        Process process,
        final fixnum.Int64 logicalClock,
      ) async {
        final event = protocol.Event()
          ..system = testInfo.system.publicKey.proto
          ..process = process.proto
          ..logicalClock = logicalClock
          ..contentType = models.ContentType.contentTypePost;

        final signedEvent = await SignedEvent.fromEvent(event, testInfo.system);

        await queries.insertEvent(
          transaction,
          signedEvent,
          event,
        );
      }

      final db = await queries.createDB(sqflite.inMemoryDatabasePath);

      await db.transaction((final transaction) async {
        for (final logicalClock in [1, 2, 3, 5, 10, 11]) {
          await insertTestEvent(
            transaction,
            testInfo1,
            testInfo1.process,
            fixnum.Int64(logicalClock),
          );
        }

        await insertTestEvent(
          transaction,
          testInfo1,
          testInfo1AltProcess.process,
          fixnum.Int64(3),
        );

        await insertTestEvent(
          transaction,
          testInfo2,
          testInfo2.process,
          fixnum.Int64(3),
        );

        final loadedRangesForSystem = await queries.rangesForSystem(
          transaction,
          testInfo1.system.publicKey,
          false,
        );

        final loadedRangesForSystemWithWindow = await queries.rangesForSystem(
          transaction,
          testInfo1.system.publicKey,
          true,
        );

        final expectedRangesForProcess1 = protocol.RangesForProcess()
          ..process = testInfo1.process.proto;
        expectedRangesForProcess1.ranges.addAll([
          protocol.Range()
            ..low = fixnum.Int64(1)
            ..high = fixnum.Int64(3),
          protocol.Range()
            ..low = fixnum.Int64(5)
            ..high = fixnum.Int64(5),
          protocol.Range()
            ..low = fixnum.Int64(10)
            ..high = fixnum.Int64(11),
        ]);

        final expectedRangesForProcess2 = protocol.RangesForProcess()
          ..process = testInfo1AltProcess.process.proto;
        expectedRangesForProcess2.ranges.addAll([
          protocol.Range()
            ..low = fixnum.Int64(3)
            ..high = fixnum.Int64(3),
        ]);

        final expectedRangesForSystem = protocol.RangesForSystem();
        expectedRangesForSystem.rangesForProcesses.addAll([
          expectedRangesForProcess1,
          expectedRangesForProcess2,
        ]);

        expectedRangesForSystem.rangesForProcesses.sort((a, b) {
          return compareBuffers(
            dart_collection.UnmodifiableListView(a.process.process),
            dart_collection.UnmodifiableListView(b.process.process),
          );
        });

        loadedRangesForSystem.rangesForProcesses.sort((a, b) {
          return compareBuffers(
            dart_collection.UnmodifiableListView(a.process.process),
            dart_collection.UnmodifiableListView(b.process.process),
          );
        });

        expect(loadedRangesForSystem, expectedRangesForSystem);
        expect(loadedRangesForSystemWithWindow, expectedRangesForSystem);
      });
    });
  });
}
