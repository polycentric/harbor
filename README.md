# FUTO ID

![pipeline](https://gitlab.futo.org/polycentric/futo_id/badges/main/pipeline.svg)

FUTO ID is an identity management application written, targeting iOS, and Android, built on [Polycentric](https://gitlab.futo.org/polycentric/polycentric). Create pseudonymous identities, make claims, link identities across applications. Check out [id.futo.org](https://id.futo.org), or [docs.polycentric.io](https://docs.polycentric.io) for more information

<table border="0">
 <tr>
    <td><b><img src="readmefiles/app1.png" height="500" /></b></td>
    <td><b><img src="readmefiles/app2.png" height="500" /></b></td>
    <td><b><img src="readmefiles/app3.png" height="500" /></b></td>
    <td><b><img src="readmefiles/app4.png" height="500" /></b></td>
 </tr>
</table>

Targeting Flutter stable `3.16.4`.

[Download APK](https://gitlab.futo.org/polycentric/futo_id/-/jobs/artifacts/main/browse?job=build)
